FROM elixir:1.7.4

ENV NODE_VERSION 10.x
# ENV MIX_ENV=dev
ENV MIX_ENV=prod
ENV fs.inotify.max_user_watches=524288

RUN apt update \
    && rm -f /etc/localtime \
    && ln -fs /usr/share/zoneinfo/Asia/Tokyo /etc/localtime

RUN curl -sL https://deb.nodesource.com/setup_${NODE_VERSION} | bash \
  && apt -y install nodejs \
  && apt -y install inotify-tools \
  && apt clean

RUN mix local.hex --force && \
    mix local.rebar --force && \
    # mix archive.install --force https://github.com/phoenixframework/archives/raw/master/phoenix_new-1.2.4.ez --force
    # mix archive.install --force https://github.com/phoenixframework/archives/raw/master/phx_new-1.3.2.ez --force
    mix archive.install hex phx_new 1.4.0 --force

CMD mix phx.server

WORKDIR /app
